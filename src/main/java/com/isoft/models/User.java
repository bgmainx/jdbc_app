package com.isoft.models;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Borimir Georgiev
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class User
{
    private int id;
    private String username;
    private String firstname;
    private String middlename;
    private String lastname;
    private int age;
    private String country;

    public User() { }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public void setMiddlename(String middlename)
    {
        this.middlename = middlename;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getUsername()
    {
        return username;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public String getMiddlename()
    {
        return middlename;
    }

    public String getLastname()
    {
        return lastname;
    }

    public int getAge()
    {
        return age;
    }

    public String getCountry()
    {
        return country;
    }

    public int getId()
    {
        return id;
    }
}
