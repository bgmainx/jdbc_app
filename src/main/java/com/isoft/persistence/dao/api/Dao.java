package com.isoft.persistence.dao.api;

import java.util.List;

/**
 * @author Borimir Georgiev
 */
public interface Dao<T, K>
{
    T get(K key);

    List<T> getAll();

    void save(T t);

    void update(T t);

    void delete(T t);
}
