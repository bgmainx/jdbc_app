-- Table: public.users_matches

-- DROP TABLE public.users_matches;

CREATE TABLE public.users_matches
(
    id SERIAL NOT NULL,
    user_id integer NOT NULL,
    match_id integer NOT NULL,
    prediction "char" NOT NULL,
    CONSTRAINT users_matches_pkey PRIMARY KEY (id),
    CONSTRAINT match_id FOREIGN KEY (match_id)
        REFERENCES public.matches (match_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT users_matches_match_id_fkey FOREIGN KEY (match_id)
        REFERENCES public.matches (match_id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT users_matches_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (user_id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE CASCADE
)
    WITH (
        OIDS=FALSE
    );
ALTER TABLE public.users_matches
    OWNER TO dedicated;
