package com.isoft.models;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Borimir Georgiev
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Match
{
    private int id;
    private String homeTeam;
    private String awayTeam;

    public Match(){}

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getHomeTeam()
    {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam)
    {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam()
    {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam)
    {
        this.awayTeam = awayTeam;
    }
}
