package com.isoft.persistence.dao.facade;

import com.isoft.models.Match;
import com.isoft.models.Prediction;
import com.isoft.models.User;
import com.isoft.persistence.dao.api.Dao;
import com.isoft.persistence.dao.impl.MatchDao;
import com.isoft.persistence.dao.impl.PredictionDao;
import com.isoft.persistence.dao.impl.UserDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Borimir Georgiev
 */
public class Persister
{
    private Map<Class, Dao> daos = new HashMap<>();

    public Persister()
    {
        daos.put(Match.class, new MatchDao());
        daos.put(User.class, new UserDao());
        daos.put(Prediction.class, new PredictionDao());
    }

    public <T, K> List<T> getAll(Class<T> clazz)
    {
        Dao<T, K> dao = getDao(clazz);
        return dao.getAll();
    }

    private <T, K> Dao<T, K> getDao(Class<T> clazz)
    {
        Dao<T, K> dao = daos.get(clazz);

        if(dao == null)
        {
            dao = // new ...
                    daos.put(clazz, dao);
        }
        return dao;
    }

    public <T, K> T get(Class<T> clazz, K key)
    {
        // get dao by T
        // call get on dao
        Dao<T, K> dao = daos.get(clazz.getClass());

        return dao.get(key);
    }

    public <T, K> void delete(Class<T> clazz, K elem)
    {
        Dao<T, K> dao = daos.get(clazz);

        dao.delete((T) elem);
    }

    public <T, K> void save(T clazz)
    {
        Dao<T, K> dao = daos.get(clazz.getClass());

        dao.save(clazz);
    }

    public <T, K> void update(Class<T> clazz, K elem)
    {
        Dao<T, K> dao = daos.get(clazz);

        dao.update((T) elem);
    }
}
