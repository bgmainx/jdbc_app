-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    user_id integer NOT NULL DEFAULT nextval('users_user_id_seq'::regclass),
    username character varying(30) NOT NULL,
    firstname character varying(30) NOT NULL,
    middlename character varying(30) NOT NULL,
    lastname character varying(30) NOT NULL,
    age smallint NOT NULL,
    country character varying(30) NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (user_id)
)
    WITH (
        OIDS=FALSE
    );
ALTER TABLE public.users
    OWNER TO dedicated;
