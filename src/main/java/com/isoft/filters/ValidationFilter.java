package com.isoft.filters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.utils.Constants;
import com.sun.xml.internal.ws.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * @author Borimir Georgiev
 */
public class ValidationFilter implements Filter
{
    final Logger LOGGER = LoggerFactory.getLogger(ValidationFilter.class);
    private ObjectMapper mapper = new ObjectMapper();


    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String modelName = getModelName(httpRequest);

        if (httpRequest.getMethod().equalsIgnoreCase("POST")
            || httpRequest.getMethod().equalsIgnoreCase("DELETE"))
        {
            try
            {
                BufferedReader r = httpRequest.getReader();
                String line;
                StringBuilder sb = new StringBuilder();
                while ((line = r.readLine()) != null)
                {
                    sb.append(line);
                }

                mapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);


                Object obj = mapper.readValue(sb.toString(), Class.forName("com.isoft.models." + modelName));

                httpRequest.setAttribute(modelName, obj);
            } catch (IOException ioe)
            {
                LOGGER.error(Constants.MAP_ERROR, ioe);

                httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            } catch (ClassNotFoundException cnfe)
            {
                LOGGER.error("Class not found.", cnfe);
                httpResponse.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                return;
            }
        }

        chain.doFilter(httpRequest, response);
    }

    private String getModelName(HttpServletRequest httpRequest)
    {
        String clazzName = httpRequest.getRequestURL().substring(httpRequest.getRequestURL().lastIndexOf("/") + 1,
                httpRequest.getRequestURL().toString().length() - 1);

        if (clazzName.endsWith("e"))
        {
            clazzName = httpRequest.getRequestURL().substring(httpRequest.getRequestURL().lastIndexOf("/") + 1,
                    httpRequest.getRequestURL().toString().length() - 2);
        }

        return StringUtils.capitalize(clazzName);
    }

    @Override
    public void destroy()
    {

    }
}
