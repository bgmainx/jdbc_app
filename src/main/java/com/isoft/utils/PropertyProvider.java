package com.isoft.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Borimir Georgiev
 */
public class PropertyProvider
{
    private Properties prop = null;
    private final Logger LOGGER = LoggerFactory.getLogger(PropertyProvider.class);


    public PropertyProvider(String path)
    {
        InputStream stream;

        try
        {
            this.prop = new Properties();
            stream = this.getClass().getResourceAsStream("/" + path);
            prop.load(stream);
        } catch (FileNotFoundException fnfe)
        {
            LOGGER.error(Constants.FILE_NOT_FOUND, fnfe);

        } catch (IOException ioe)
        {
            LOGGER.error(Constants.IO_STREAM, ioe);
        }
    }

    public String getPropertyValue(String key)
    {
        return this.prop.getProperty(key);
    }

}
