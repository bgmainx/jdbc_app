package com.isoft.persistence.dao.impl;

import com.isoft.models.Prediction;
import com.isoft.persistence.connection.JDBConnection;
import com.isoft.persistence.dao.api.Dao;
import com.isoft.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Borimir Georgiev
 */
public class PredictionDao implements Dao<Prediction, Integer>
{
    private ResultSet rs;
    private final Logger LOGGER = LoggerFactory.getLogger(PredictionDao.class);

    @Override
    public Prediction get(Integer id)
    {
        Prediction prediction = new Prediction();

        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM users_matches WHERE users_matches_id=?"))
        {
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next())
            {
                prediction.setId(rs.getInt(1));
                prediction.setHomeTeam(rs.getString(2));
                prediction.setAwayTeam(rs.getString(3));
                prediction.setPrediction(rs.getString(4));
            }
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }

        return prediction;
    }

    @Override
    public List<Prediction> getAll()
    {
        List<Prediction> predictions = new ArrayList<>();

        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("SELECT users_matches_id, users.username, matches.home_team, matches.away_team, prediction\n" +
                "FROM users_matches\n" +
                "JOIN users ON users.user_id = users_matches.user_id\n" +
                "JOIN matches ON matches.match_id = users_matches.match_id"))
        {
            rs = ps.executeQuery();

            while (rs.next())
            {
                Prediction prediction = new Prediction();

                prediction.setId(rs.getInt(1));
                prediction.setUsername(rs.getString(2));
                prediction.setHomeTeam(rs.getString(3));
                prediction.setAwayTeam(rs.getString(4));
                prediction.setPrediction(rs.getString(5));

                predictions.add(prediction);
            }

        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }

        return predictions;
    }

    @Override
    public void save(Prediction prediction)
    {
        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection
                     .prepareStatement("INSERT INTO users_matches (user_id, match_id, prediction) \n" +
                             "VALUES((SELECT user_id FROM users WHERE username=?),\n" +
                             "(SELECT match_id FROM matches WHERE home_team LIKE ? AND away_team LIKE ?)\n" +
                             ", ?);"))
        {
            ps.setString(1, prediction.getUsername());
            ps.setString(2, prediction.getHomeTeam());
            ps.setString(3, prediction.getAwayTeam());
            ps.setString(4, prediction.getPrediction());

            ps.executeUpdate();
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }
    }

    @Override
    public void update(Prediction prediction)
    {
        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection
                .prepareStatement("UPDATE users_matches SET prediction=? WHERE users_matches_id=?"))
        {

            ps.setString(1, prediction.getPrediction());
            ps.setInt(2, prediction.getId());

            ps.executeUpdate();
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }
    }

    @Override
    public void delete(Prediction prediction)
    {
        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("DELETE FROM users_matches WHERE users_matches_id=?"))
        {

            ps.setInt(1, prediction.getId());

            ps.executeUpdate();
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }
    }
}
