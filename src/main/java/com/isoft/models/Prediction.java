package com.isoft.models;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Borimir Georgiev
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Prediction
{
    private int id;
    private String username;
    private String homeTeam;
    private String awayTeam;
    private String prediction;

    public Prediction()
    {

    }

    public int getId()
    {
        return id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getHomeTeam()
    {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam)
    {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam()
    {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam)
    {
        this.awayTeam = awayTeam;
    }

    public String getPrediction()
    {
        return prediction;
    }

    public void setPrediction(String prediction)
    {
        this.prediction = prediction;
    }
}
