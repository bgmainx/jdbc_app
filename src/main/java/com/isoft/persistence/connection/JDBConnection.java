package com.isoft.persistence.connection;

import com.isoft.utils.PropertyProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Borimir Georgiev
 */
public final class JDBConnection
{
    private static PropertyProvider property = new PropertyProvider("dbConnection.properties");
    private static String url = property.getPropertyValue("db.conn.url");
    private static String username = property.getPropertyValue("db.username");
    private static String password = property.getPropertyValue("db.password");

    private static final Logger LOGGER = LoggerFactory.getLogger(JDBConnection.class);

    private JDBConnection()
    {
    }

    public static Connection connect()
    {
        try
        {
            Class.forName("org.postgresql.Driver");

            Connection conn = DriverManager
                    .getConnection(url + "?user=" + username + "&password=" + password);

            LOGGER.info("Connection made.");
            return conn;
        } catch (SQLException | ClassNotFoundException cnfe)
        {
            LOGGER.error("Connection problem.", cnfe);
        }
        return null;
    }
}
