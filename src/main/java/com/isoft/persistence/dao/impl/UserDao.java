package com.isoft.persistence.dao.impl;

import com.isoft.models.User;
import com.isoft.persistence.connection.JDBConnection;
import com.isoft.persistence.dao.api.Dao;
import com.isoft.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Borimir Georgiev
 */
public class UserDao implements Dao<User, Integer>
{
    private Connection connection;
    private ResultSet rs;
    private final Logger LOGGER = LoggerFactory.getLogger(UserDao.class);

    @Override
    public User get(Integer id)
    {
        User user = new User();

        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM users WHERE user_id=?"))
        {
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next())
            {
                user.setId(rs.getInt(1));
                user.setUsername(rs.getString(2));
                user.setFirstname(rs.getString(3));
                user.setMiddlename(rs.getString(4));
                user.setLastname(rs.getString(5));
                user.setAge(rs.getInt(6));
                user.setCountry(rs.getString(7));
            }
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }

        return user;
    }

    @Override
    public List<User> getAll()
    {
        List<User> users = new ArrayList<>();

        try (Connection connection = JDBConnection.connect();
                PreparedStatement ps = connection.prepareStatement("SELECT * FROM users"))
        {
            rs = ps.executeQuery();

            while (rs.next())
            {
                User user = new User();

                user.setId(rs.getInt(1));
                user.setUsername(rs.getString(2));
                user.setFirstname(rs.getString(3));
                user.setMiddlename(rs.getString(4));
                user.setLastname(rs.getString(5));
                user.setAge(rs.getInt(6));
                user.setCountry(rs.getString(7));

                users.add(user);
            }

        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }

        return users;
    }

    @Override
    public void save(User user)
    {
        try (Connection connection = JDBConnection.connect();
                PreparedStatement ps = connection
                .prepareStatement("INSERT INTO users(username, firstname, middlename, lastname, age, country)"
                        + "VALUES (?, ?, ?, ?, ?, ?)"))
        {

            ps.setString(1, user.getUsername());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getMiddlename());
            ps.setString(4, user.getLastname());
            ps.setInt(5, user.getAge());
            ps.setString(6, user.getCountry());

            ps.executeUpdate();
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }
    }

    @Override
    public void update(User user)
    {
        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection
                .prepareStatement("UPDATE users SET username=?, firstname=?, middlename=?, lastname=?, age=?, country=?" +
                        "WHERE user_id=?"))
        {

            ps.setString(1, user.getUsername());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getMiddlename());
            ps.setString(4, user.getLastname());
            ps.setInt(5, user.getAge());
            ps.setString(6, user.getCountry());
            ps.setInt(7, user.getId());

            ps.executeUpdate();
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }
    }

    @Override
    public void delete(User user)
    {
        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("DELETE FROM users WHERE user_id=?"))
        {

            ps.setInt(1, user.getId());

            ps.executeUpdate();
        } catch (SQLException se)
        {
            LOGGER.error(Constants.SQL_ERR, se);
        }
    }
}
