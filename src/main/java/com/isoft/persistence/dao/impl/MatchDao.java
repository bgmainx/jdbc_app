package com.isoft.persistence.dao.impl;

import com.isoft.models.Match;
import com.isoft.persistence.connection.JDBConnection;
import com.isoft.persistence.dao.api.Dao;
import com.isoft.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Borimir Georgiev
 */
public class MatchDao implements Dao<Match, Integer>
{
    private ResultSet rs;
    private final Logger LOGGER = LoggerFactory.getLogger(MatchDao.class);

    @Override
    public Match get(Integer id)
    {
        Match match = new Match();

        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM matches WHERE match_id=?"))
        {
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next())
            {
                match.setId(rs.getInt(1));
                match.setHomeTeam(rs.getString(2));
                match.setAwayTeam(rs.getString(3));
            }
        } catch (SQLException e)
        {
            LOGGER.error(Constants.SQL_ERR, e);
        }

        return match;
    }


    @Override
    public List<Match> getAll()
    {
        List<Match> matches = new ArrayList<>();

        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM matches"))
        {
            rs = ps.executeQuery();

            while (rs.next())
            {
                Match match = new Match();

                match.setId(rs.getInt(1));
                match.setHomeTeam(rs.getString(2));
                match.setAwayTeam(rs.getString(3));

                matches.add(match);
            }

        } catch (SQLException e)
        {
            LOGGER.error(Constants.SQL_ERR, e);
        }

        return matches;
    }

    @Override
    public void save(Match match)
    {

        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection
                .prepareStatement("INSERT INTO matches(home_team, away_team)"
                        + "VALUES (?, ?)"))
        {

            ps.setString(1, match.getHomeTeam());
            ps.setString(2, match.getAwayTeam());

            ps.executeUpdate();
        } catch (SQLException e)
        {
            LOGGER.error(Constants.SQL_ERR, e);
        }
    }

    @Override
    public void update(Match match)
    {
        try (Connection connection = JDBConnection.connect();
                PreparedStatement ps = connection
                .prepareStatement("UPDATE matches SET home_team=?, away_team=?" +
                        "WHERE match_id=?"))
        {
            ps.setString(1, match.getHomeTeam());
            ps.setString(2, match.getAwayTeam());
            ps.setInt(3, match.getId());

            ps.executeUpdate();
        } catch (SQLException e)
        {
            LOGGER.error(Constants.SQL_ERR, e);
        }
    }

    @Override
    public void delete(Match match)
    {
        try (Connection connection = JDBConnection.connect();
             PreparedStatement ps = connection.prepareStatement("DELETE FROM matches WHERE match_id=?"))
        {

            ps.setInt(1, match.getId());

            ps.executeUpdate();
        } catch (SQLException e)
        {
            LOGGER.error(Constants.SQL_ERR, e);
        }
    }
}
