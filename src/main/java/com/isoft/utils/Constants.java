package com.isoft.utils;

/**
 * @author Borimir Georgiev
 */
public class Constants
{
    private Constants() {}

    public static final String MAP_ERROR = "Cannot map the json object.";
    public static final String FILE_NOT_FOUND = "File not found";
    public static final String IO_STREAM = "IO stream didn't work properly";
    public static final String SQL_ERR = "SQL exception. Broken SQL, missing table etc.";
    public static final String RESPONSE_ERROR = "Response exception.";

}
