-- Table: public.matches

-- DROP TABLE public.matches;

CREATE TABLE public.matches
(
    match_id integer NOT NULL DEFAULT nextval('matches_match_id_seq'::regclass),
    home_team character varying(30) NOT NULL,
    away_team character varying(30) NOT NULL,
    CONSTRAINT matches_pkey PRIMARY KEY (match_id)
)
    WITH (
        OIDS=FALSE
    );
ALTER TABLE public.matches
    OWNER TO dedicated;
