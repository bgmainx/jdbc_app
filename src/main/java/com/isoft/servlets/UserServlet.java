package com.isoft.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.isoft.models.User;
import com.isoft.persistence.dao.facade.Persister;
import com.isoft.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;

/**
 * @author Borimir Georgiev
 */
public class UserServlet extends HttpServlet
{
    private static Persister persister = new Persister();
    private static String json;
    private static PrintWriter out;
    private static ObjectMapper mapper = new ObjectMapper();
    private final Logger LOGGER = LoggerFactory.getLogger(UserServlet.class);


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        User user = (User) request.getAttribute("User");
        persister.save(user);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        response.setContentType("application/json");

        List<User> users = persister.getAll(User.class);


        try
        {
            json = mapper.writeValueAsString(users);
            out = response.getWriter();
        } catch (IOException ioe)
        {
            LOGGER.error(Constants.MAP_ERROR, ioe);

            try
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e)
            {
                LOGGER.error(Constants.RESPONSE_ERROR, e);
            }
        }

        out.println(json);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            User user = mapper.readValue(request.getReader(), User.class);
            persister.delete(User.class, user);
        } catch (IOException ioe)
        {
            LOGGER.error(Constants.MAP_ERROR, ioe);

            try
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e)
            {
                LOGGER.error(Constants.RESPONSE_ERROR, e);
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            User user = mapper.readValue(request.getReader(), User.class);
            persister.update(User.class, user);
        } catch (IOException ioe)
        {
            LOGGER.error(Constants.MAP_ERROR, ioe);

            try
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e)
            {
                LOGGER.error(Constants.RESPONSE_ERROR, e);
            }
        }
    }
}
