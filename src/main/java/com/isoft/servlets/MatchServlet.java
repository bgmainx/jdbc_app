package com.isoft.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.models.Match;
import com.isoft.persistence.dao.facade.Persister;
import com.isoft.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Borimir Georgiev
 */
public class MatchServlet extends HttpServlet
{
    private Persister persister = new Persister();
    private String json;
    private PrintWriter out;
    private ObjectMapper mapper = new ObjectMapper();

    final Logger LOGGER = LoggerFactory.getLogger(MatchServlet.class);


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        Match match = (Match) request.getAttribute("Match");
        persister.save(match);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        response.setContentType("application/json");

        List<Match> matches = persister.getAll(Match.class);

        try
        {
            json = mapper.writeValueAsString(matches);
            out = response.getWriter();
        } catch (IOException ioe)
        {
            LOGGER.error(Constants.MAP_ERROR, ioe);

            try
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e)
            {
                LOGGER.error(Constants.RESPONSE_ERROR, e);
            }
        }

        out.println(json);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            Match match = mapper.readValue(request.getReader(), Match.class);
            persister.delete(Match.class, match);
        } catch (IOException ioe)
        {
            LOGGER.error(Constants.MAP_ERROR, ioe);

            try
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e)
            {
                LOGGER.error(Constants.RESPONSE_ERROR, e);
            }
        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            Match match = mapper.readValue(request.getReader(), Match.class);
            persister.update(Match.class, match);
        } catch (IOException ioe)
        {
            LOGGER.error(Constants.MAP_ERROR, ioe);

            try
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e)
            {
                LOGGER.error(Constants.RESPONSE_ERROR, e);
            }
        }
    }
}
